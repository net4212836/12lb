﻿namespace WebApplication17.Models
{
    public class CompanyViewModel
    {
        public int Id { get; set; }
        public string? Name { get; set; }
        public string? Description { get; set; }
        public int NumWorkers { get; set; }
        public string? Location { get; set; }

        public CompanyViewModel(string name, string description, int numWorkers, string location)
        {
            Name = name;
            Description = description;
            NumWorkers = numWorkers;
            Location = location;
        }
    }
}
