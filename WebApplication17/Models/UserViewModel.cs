﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace WebApplication17.Models
{
	public class UserViewModel
	{
		public int Id { get; set; }
		[Required]
		[DisplayName("First Name")]
		public string? FirstName { get; set; }
		[Required]
		[DisplayName("Last Name")]
		public string? LastName { get; set; }
		[Required]
		[DisplayName("Age")]
		public int Age { get; set; }

        public override string ToString()
        {
            return FirstName +" "+ LastName + " Age:" + Age;
        }
    }
}
