﻿using Microsoft.EntityFrameworkCore;

namespace WebApplication17.Models
{
	public class ApplicationContext : DbContext
	{
		public DbSet<UserViewModel> Users { get; set; }
        public DbSet<CompanyViewModel> Companies { get; set; }

        public ApplicationContext(DbContextOptions<ApplicationContext> options) : base(options)
		{
			
		}
	}
}
