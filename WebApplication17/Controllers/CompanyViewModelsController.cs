﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using WebApplication17.Models;

namespace WebApplication17.Controllers
{
    public class CompanyViewModelsController : Controller
    {
        private readonly ApplicationContext _context;
        private readonly List<CompanyViewModel> Companies = new List<CompanyViewModel>() {
        new CompanyViewModel("TechPulse Solutions", "echPulse Solutions is a cutting-edge technology firm specializing in artificial intelligence and machine learning solutions. We develop innovative software applications and provide consulting services to help businesses leverage the power of AI for growth and efficiency.", 120, "Silicon Valley, California, USA" ),
        new CompanyViewModel( "EcoHarvest Farms", "EcoHarvest Farms is a sustainable agriculture company committed to providing organic and locally sourced produce. We employ advanced farming techniques and environmentally friendly practices to ensure a healthy and eco-conscious food supply chain.", 75, "Vancouver Island, British Columbia, Canada"),
        new CompanyViewModel( "BioHealth Innovations", "BioHealth Innovations is a biotechnology company dedicated to advancing healthcare through the development of cutting-edge pharmaceuticals and medical technologies. Our focus is on improving patient outcomes and addressing unmet medical needs.", 200, "Cambridge, England"),
        new CompanyViewModel("SolarSprint Energy", "SolarSprint Energy is a renewable energy company specializing in solar power solutions. We design and implement solar energy systems for residential, commercial, and industrial clients, contributing to a sustainable and clean energy future.", 90, "Austin, Texas, USA"),
        new CompanyViewModel("ArtisanCraft Studios", " ArtisanCraft Studios is a creative design agency that specializes in crafting unique and visually stunning digital content. Our team of artists and designers collaborate to deliver engaging graphics, animations, and branding solutions for clients across various industries.", 50, "Tokyo, Japan")
        };

        public CompanyViewModelsController(ApplicationContext context)
        {
            _context = context;
        }

        public async Task<IActionResult> Index()
        {
            //foreach (var company in Companies)
            //{
            //    _context.Add(company);
            //    await _context.SaveChangesAsync();
            //}
              return View(await _context.Companies.ToListAsync());
        }
    }
}
